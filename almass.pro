#-------------------------------------------------
#
# Project created by QtCreator 2018-06-21T13:49:54
#
#-------------------------------------------------

QT       += core gui
#QT += charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#greaterThan(QT_MAJOR_VERSION, 5): QT += widgets printsupport

#INCLUDEPATH += /usr/include/qwt

#unix {
#LIBS += -L/usr/lib -lqwt-qt4
#LIBS += -L/usr/local/lib -lQtSvg
#}

CONFIG += c++11

TARGET = almass
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++11 -fPIC
CXXFLAGS+="-D__STRICT_ANSI__"
#CXXFLAGS+= -std=c++11
#CXXFLAGS+=-Werror=return-type

#QMAKE_CXXFLAGS += cxx_lib_env=host

SOURCES += ../ALMaSS_all/qt/main.cpp\
        ../ALMaSS_all/qt/mainwindow.cpp \
    ../ALMaSS_all/BatchALMaSS/AOR_Probe.cpp \
    ../ALMaSS_all/BatchALMaSS/BinaryMapBase.cpp \
    ../ALMaSS_all/BatchALMaSS/CurveClasses.cpp \
    ../ALMaSS_all/BatchALMaSS/MovementMap.cpp \
    ../ALMaSS_all/BatchALMaSS/PopulationManager.cpp \
    ../ALMaSS_all/BatchALMaSS/PositionMap.cpp \
    ../ALMaSS_all/Bembidion/Bembidion_all.cpp \
    ../ALMaSS_all/Dormouse/Dormouse_Population_Manager.cpp \
    ../ALMaSS_all/Dormouse/Dormouse.cpp \
    ../ALMaSS_all/GooseManagement/Goose_Base.cpp \
    ../ALMaSS_all/GooseManagement/Goose_Population_Manager.cpp \
    ../ALMaSS_all/GooseManagement/GooseBarnacle_All.cpp \
    ../ALMaSS_all/GooseManagement/GooseGreylag_All.cpp \
    ../ALMaSS_all/GooseManagement/GooseMemoryMap.cpp \
    ../ALMaSS_all/GooseManagement/GoosePinkFooted_All.cpp \
    ../ALMaSS_all/Hare/Hare_all.cpp \
    ../ALMaSS_all/Hare/Hare_THare.cpp \
    ../ALMaSS_all/Hare/HareForagenPeg.cpp \
    ../ALMaSS_all/HoneyBee/HoneyBee_Colony.cpp \
    ../ALMaSS_all/HoneyBee/HoneyBee.cpp \
    ../ALMaSS_all/Hunters/Hunters_all.cpp \
    ../ALMaSS_all/Landscape/cropprogs/AgroChemIndustryCereal.cpp \
    ../ALMaSS_all/Landscape/cropprogs/BroadBeans.cpp \
    ../ALMaSS_all/Landscape/cropprogs/Carrots.cpp \
    ../ALMaSS_all/Landscape/cropprogs/CloverGrassGrazed1.cpp \
    ../ALMaSS_all/Landscape/cropprogs/CloverGrassGrazed2.cpp \
    ../ALMaSS_all/Landscape/cropprogs/DummyCropPestTesting.cpp \
    ../ALMaSS_all/Landscape/cropprogs/FieldPeas.cpp \
    ../ALMaSS_all/Landscape/cropprogs/FieldPeasSilage.cpp \
    ../ALMaSS_all/Landscape/cropprogs/FieldPeasStrigling.cpp \
    ../ALMaSS_all/Landscape/cropprogs/Fodderbeet.cpp \
    ../ALMaSS_all/Landscape/cropprogs/FodderGrass.cpp \
    ../ALMaSS_all/Landscape/cropprogs/Maize.cpp \
    ../ALMaSS_all/Landscape/cropprogs/MaizeSilage.cpp \
    ../ALMaSS_all/Landscape/cropprogs/MaizeStrigling.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NorwegianOats.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NorwegianPotatoes.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NorwegianSpringBarley.cpp \
    ../ALMaSS_all/Landscape/cropprogs/Oats.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OBarleyPeaCloverGrass.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OCarrots.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OCloverGrassGrazed1.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OCloverGrassGrazed2.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OCloverGrassSilage1.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OFieldPeas.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OFieldPeasSilage.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OFirstYearDanger.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OFodderbeet.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OGrazingPigs.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OMaizeSilage.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OOats.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OPermanentGrassGrazed.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OPotatoes.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OrchardCrop.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OSBarleySilage.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OSeedGrass1.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OSeedGrass2.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OSpringBarley.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OSpringBarleyExt.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OSpringBarleyPigs.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OTriticale.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OWinterBarley.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OWinterBarleyExt.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OWinterRape.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OWinterRye.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OWinterWheat.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OWinterWheatUndersown.cpp \
    ../ALMaSS_all/Landscape/cropprogs/OWinterWheatUndersownExt.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PermanentGrassGrazed.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PermanentGrassLowYield.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PermanentGrassTussocky.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PermanentSetAside.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PLBeans.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PLBeet.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PLBeetSpr.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PLCarrots.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PLFodderLucerne1.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PLFodderLucerne2.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PLMaize.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PLMaizeSilage.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PLPotatoes.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PLSpringBarley.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PLSpringBarleySpr.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PLSpringWheat.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PLWinterBarley.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PLWinterRape.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PLWinterRye.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PLWinterTriticale.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PLWinterWheat.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PLWinterWheatLate.cpp \
    ../ALMaSS_all/Landscape/cropprogs/Potatoes.cpp \
    ../ALMaSS_all/Landscape/cropprogs/PotatoesIndustry.cpp \
    ../ALMaSS_all/Landscape/cropprogs/seedgrass1.cpp \
    ../ALMaSS_all/Landscape/cropprogs/seedgrass2.cpp \
    ../ALMaSS_all/Landscape/cropprogs/setaside.cpp \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarley.cpp \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleyCloverGrass.cpp \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleyCloverGrassStrigling.cpp \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleyPeaCloverGrassStrigling.cpp \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleyPTreatment.cpp \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleySeed.cpp \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleySilage.cpp \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleySKManagement.cpp \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleySpr.cpp \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleyStrigling.cpp \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleyStriglingCulm.cpp \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleyStriglingSingle.cpp \
    ../ALMaSS_all/Landscape/cropprogs/SpringRape.cpp \
    ../ALMaSS_all/Landscape/cropprogs/Sugarbeet.cpp \
    ../ALMaSS_all/Landscape/cropprogs/Triticale.cpp \
    ../ALMaSS_all/Landscape/cropprogs/WinterBarley.cpp \
    ../ALMaSS_all/Landscape/cropprogs/WinterBarleyStrigling.cpp \
    ../ALMaSS_all/Landscape/cropprogs/winterrape.cpp \
    ../ALMaSS_all/Landscape/cropprogs/WinterRapeStrigling.cpp \
    ../ALMaSS_all/Landscape/cropprogs/WinterRye.cpp \
    ../ALMaSS_all/Landscape/cropprogs/WinterRyeStrigling.cpp \
    ../ALMaSS_all/Landscape/cropprogs/WinterWheat.cpp \
    ../ALMaSS_all/Landscape/cropprogs/WinterWheatStrigling.cpp \
    ../ALMaSS_all/Landscape/cropprogs/WinterWheatStriglingCulm.cpp \
    ../ALMaSS_all/Landscape/cropprogs/WinterWheatStriglingSingle.cpp \
    ../ALMaSS_all/Landscape/cropprogs/WWheatPControl.cpp \
    ../ALMaSS_all/Landscape/cropprogs/WWheatPToxicControl.cpp \
    ../ALMaSS_all/Landscape/cropprogs/WWheatPTreatment.cpp \
    ../ALMaSS_all/Landscape/cropprogs/YoungForest.cpp \
    ../ALMaSS_all/MarshFritillary/MarshFritillary_Adult.cpp \
    ../ALMaSS_all/MarshFritillary/MarshFritillary_Base.cpp \
    ../ALMaSS_all/MarshFritillary/MarshFritillary_Egg.cpp \
    ../ALMaSS_all/MarshFritillary/MarshFritillary_Female.cpp \
    ../ALMaSS_all/MarshFritillary/MarshFritillary_Larvae.cpp \
    ../ALMaSS_all/MarshFritillary/MarshFritillary_Male.cpp \
    ../ALMaSS_all/MarshFritillary/MarshFritillary_Population_Manager.cpp \
    ../ALMaSS_all/MarshFritillary/MarshFritillary_Pupae.cpp \
    ../ALMaSS_all/Newt/Newt_Population_Manager.cpp \
    ../ALMaSS_all/Newt/Newt.cpp \
    ../ALMaSS_all/OliveMoth/lacewing.cpp \
    ../ALMaSS_all/OliveMoth/olivemoth.cpp \
    ../ALMaSS_all/Osmia/Osmia_Population_Manager.cpp \
    ../ALMaSS_all/Osmia/Osmia.cpp \
    ../ALMaSS_all/Partridge/Partridge_All.cpp \
    ../ALMaSS_all/Partridge/Partridge_Communication.cpp \
    ../ALMaSS_all/Partridge/Partridge_Covey.cpp \
    ../ALMaSS_all/Partridge/Partridge_Population_Manager.cpp \
    ../ALMaSS_all/Pipistrelle/Pipistrelle_Population_Manager.cpp \
    ../ALMaSS_all/Pipistrelle/Pipistrelle.cpp \
    ../ALMaSS_all/Rabbit/Rabbit_Population_Manager.cpp \
    ../ALMaSS_all/Rabbit/Rabbit.cpp \
    ../ALMaSS_all/RodenticideModelling/Rodenticide.cpp \
    ../ALMaSS_all/RodenticideModelling/RodenticidePredators.cpp \
    ../ALMaSS_all/RoeDeer/Roe_adult.cpp \
    ../ALMaSS_all/RoeDeer/Roe_base.cpp \
    ../ALMaSS_all/RoeDeer/Roe_constants.cpp \
    ../ALMaSS_all/RoeDeer/Roe_fawn.cpp \
    ../ALMaSS_all/RoeDeer/Roe_female.cpp \
    ../ALMaSS_all/RoeDeer/Roe_grid.cpp \
    ../ALMaSS_all/RoeDeer/Roe_male.cpp \
    ../ALMaSS_all/RoeDeer/Roe_Population_Manager.cpp \
    ../ALMaSS_all/RoeDeer/Roe_range_qual.cpp \
    ../ALMaSS_all/Skylark/SkylarkConfigs.cpp \
    ../ALMaSS_all/Skylark/skylarks_all.cpp \
    ../ALMaSS_all/Spider/Spider_All.cpp \
    ../ALMaSS_all/Spider/SpiderPopulationManager.cpp \
    ../ALMaSS_all/Vole/GeneticMaterial.cpp \
    ../ALMaSS_all/Vole/Predators.cpp \
    ../ALMaSS_all/Vole/Vole_all.cpp \
    ../ALMaSS_all/Vole/VolePopulationManager.cpp \
    ../ALMaSS_all/Landscape/calendar.cpp \
    ../ALMaSS_all/Landscape/configurator.cpp \
    ../ALMaSS_all/Landscape/croprotation.cpp \
    ../ALMaSS_all/Landscape/elements.cpp \
    ../ALMaSS_all/Landscape/farm.cpp \
    ../ALMaSS_all/Landscape/farmfuncs.cpp \
    ../ALMaSS_all/Landscape/hedgebanks.cpp \
    ../ALMaSS_all/Landscape/Landscape.cpp \
    ../ALMaSS_all/Landscape/map_cfg.cpp \
    ../ALMaSS_all/Landscape/maperrormsg.cpp \
    ../ALMaSS_all/Landscape/misc.cpp \
    ../ALMaSS_all/Landscape/pesticide.cpp \
    ../ALMaSS_all/Landscape/plants.cpp \
    ../ALMaSS_all/Landscape/rastermap.cpp \
    ../ALMaSS_all/Landscape/weather.cpp \
    ../ALMaSS_all/qt/almass_support.cpp \
    ../ALMaSS_all/qt/habitatmap.cpp \
    ../ALMaSS_all/qt/clickablelabel.cpp \
    ../ALMaSS_all/qt/mwhivetool.cpp \
    ../ALMaSS_all/qt/honeycombcell.cpp \
    ../ALMaSS_all/HoneyBee/hive.cpp \
    ../ALMaSS_all/HoneyBee/testlandscape.cpp \
    ../ALMaSS_all/qt/resourcebrowser.cpp \
    ../ALMaSS_all/HoneyBee/hbtools.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NLBeet.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NLBeetSpring.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NLCabbage.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NLCabbageSpring.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NLCarrots.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NLCarrotsSpring.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NLCatchPeaCrop.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NLGrassGrazed1.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NLGrassGrazed1Spring.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NLGrassGrazed2.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NLGrassGrazedLast.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NLMaize.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NLMaizeSpring.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NLPermanentGrassGrazed.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NLPotatoes.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NLPotatoesSpring.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NLSpringBarley.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NLSpringBarleySpring.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NLTulips.cpp \
    ../ALMaSS_all/Landscape/cropprogs/NLWinterWheat.cpp \
    ../ALMaSS_all/qt/colmap.cpp \
    ../ALMaSS_all/HoneyBee/hbspreadsheet.cpp \
    ../ALMaSS_all/HoneyBee/hbweather.cpp

HEADERS  += ../ALMaSS_all/qt/mainwindow.h \
    ../ALMaSS_all/BatchALMaSS/ALMaSS_Setup.h \
    ../ALMaSS_all/BatchALMaSS/AOR_Probe.h \
    ../ALMaSS_all/BatchALMaSS/BinaryMapBase.h \
    ../ALMaSS_all/BatchALMaSS/BoostRandomGenerators.h \
    ../ALMaSS_all/BatchALMaSS/BoostRandomGenerators1.h \
    ../ALMaSS_all/BatchALMaSS/CurveClasses.h \
    ../ALMaSS_all/BatchALMaSS/MovementMap.h \
    ../ALMaSS_all/BatchALMaSS/PopulationManager.h \
    ../ALMaSS_all/BatchALMaSS/positionmap.h \
    ../ALMaSS_all/BatchALMaSS/References_ODDox.h \
    ../ALMaSS_all/Bembidion/bembidion_all.h \
    ../ALMaSS_all/Dormouse/Dormouse_Population_Manager.h \
    ../ALMaSS_all/Dormouse/Dormouse.h \
    ../ALMaSS_all/GooseManagement/Goose_Base.h \
    ../ALMaSS_all/GooseManagement/Goose_Population_Manager.h \
    ../ALMaSS_all/GooseManagement/GooseBarnacle_All.h \
    ../ALMaSS_all/GooseManagement/GooseGreylag_All.h \
    ../ALMaSS_all/GooseManagement/GooseMemoryMap.h \
    ../ALMaSS_all/GooseManagement/GoosePinkFooted_All.h \
    ../ALMaSS_all/Hare/hare_all.h \
    ../ALMaSS_all/HoneyBee/BeeVirusInfectionLevels.h \
    ../ALMaSS_all/HoneyBee/HoneyBee_Colony.h \
    ../ALMaSS_all/HoneyBee/HoneyBee.h \
    ../ALMaSS_all/Hunters/Hunters_all.h \
    ../ALMaSS_all/Landscape/cropprogs/AgroChemIndustryCereal.h \
    ../ALMaSS_all/Landscape/cropprogs/BarleyPeaCloverGrassStrigling.h \
    ../ALMaSS_all/Landscape/cropprogs/BroadBeans.h \
    ../ALMaSS_all/Landscape/cropprogs/Carrots.h \
    ../ALMaSS_all/Landscape/cropprogs/CloverGrassGrazed1.h \
    ../ALMaSS_all/Landscape/cropprogs/CloverGrassGrazed2.h \
    ../ALMaSS_all/Landscape/cropprogs/dummy.h \
    ../ALMaSS_all/Landscape/cropprogs/DummyCropPestTesting.h \
    ../ALMaSS_all/Landscape/cropprogs/FieldPeas.h \
    ../ALMaSS_all/Landscape/cropprogs/FieldPeasSilage.h \
    ../ALMaSS_all/Landscape/cropprogs/FieldPeasStrigling.h \
    ../ALMaSS_all/Landscape/cropprogs/Fodderbeet.h \
    ../ALMaSS_all/Landscape/cropprogs/FodderGrass.h \
    ../ALMaSS_all/Landscape/cropprogs/Maize.h \
    ../ALMaSS_all/Landscape/cropprogs/MaizeSilage.h \
    ../ALMaSS_all/Landscape/cropprogs/MaizeStrigling.h \
    ../ALMaSS_all/Landscape/cropprogs/NorwegianOats.h \
    ../ALMaSS_all/Landscape/cropprogs/NorwegianPotatoes.h \
    ../ALMaSS_all/Landscape/cropprogs/NorwegianSpringBarley.h \
    ../ALMaSS_all/Landscape/cropprogs/Oats.h \
    ../ALMaSS_all/Landscape/cropprogs/OBarleyPeaCloverGrass.h \
    ../ALMaSS_all/Landscape/cropprogs/OCarrots.h \
    ../ALMaSS_all/Landscape/cropprogs/OCloverGrassGrazed1.h \
    ../ALMaSS_all/Landscape/cropprogs/OCloverGrassGrazed2.h \
    ../ALMaSS_all/Landscape/cropprogs/OCloverGrassSilage1.h \
    ../ALMaSS_all/Landscape/cropprogs/OFieldPeas.h \
    ../ALMaSS_all/Landscape/cropprogs/OFieldPeasSilage.h \
    ../ALMaSS_all/Landscape/cropprogs/OFirstYearDanger.h \
    ../ALMaSS_all/Landscape/cropprogs/OFodderbeet.h \
    ../ALMaSS_all/Landscape/cropprogs/OGrazingPigs.h \
    ../ALMaSS_all/Landscape/cropprogs/OMaizeSilage.h \
    ../ALMaSS_all/Landscape/cropprogs/OOats.h \
    ../ALMaSS_all/Landscape/cropprogs/OPermanentGrassGrazed.h \
    ../ALMaSS_all/Landscape/cropprogs/OPotatoes.h \
    ../ALMaSS_all/Landscape/cropprogs/OrchardCrop.h \
    ../ALMaSS_all/Landscape/cropprogs/OSBarleySilage.h \
    ../ALMaSS_all/Landscape/cropprogs/OSeedGrass1.h \
    ../ALMaSS_all/Landscape/cropprogs/OSeedGrass2.h \
    ../ALMaSS_all/Landscape/cropprogs/OSpringBarley.h \
    ../ALMaSS_all/Landscape/cropprogs/OSpringBarleyExt.h \
    ../ALMaSS_all/Landscape/cropprogs/OSpringBarleyPigs.h \
    ../ALMaSS_all/Landscape/cropprogs/OTriticale.h \
    ../ALMaSS_all/Landscape/cropprogs/OWinterBarley.h \
    ../ALMaSS_all/Landscape/cropprogs/OWinterBarleyExt.h \
    ../ALMaSS_all/Landscape/cropprogs/OWinterRape.h \
    ../ALMaSS_all/Landscape/cropprogs/OWinterRye.h \
    ../ALMaSS_all/Landscape/cropprogs/OWinterWheat.h \
    ../ALMaSS_all/Landscape/cropprogs/OWinterWheatUndersown.h \
    ../ALMaSS_all/Landscape/cropprogs/OWinterWheatUndersownExt.h \
    ../ALMaSS_all/Landscape/cropprogs/PermanentGrassGrazed.h \
    ../ALMaSS_all/Landscape/cropprogs/PermanentGrassLowYield.h \
    ../ALMaSS_all/Landscape/cropprogs/PermanentGrassTussocky.h \
    ../ALMaSS_all/Landscape/cropprogs/PermanentSetAside.h \
    ../ALMaSS_all/Landscape/cropprogs/PLBeans.h \
    ../ALMaSS_all/Landscape/cropprogs/PLBeet.h \
    ../ALMaSS_all/Landscape/cropprogs/PLBeetSpr.h \
    ../ALMaSS_all/Landscape/cropprogs/PLCarrots.h \
    ../ALMaSS_all/Landscape/cropprogs/PLFodderLucerne1.h \
    ../ALMaSS_all/Landscape/cropprogs/PLFodderLucerne2.h \
    ../ALMaSS_all/Landscape/cropprogs/PLMaize.h \
    ../ALMaSS_all/Landscape/cropprogs/PLMaizeSilage.h \
    ../ALMaSS_all/Landscape/cropprogs/PLPotatoes.h \
    ../ALMaSS_all/Landscape/cropprogs/PLSpringBarley.h \
    ../ALMaSS_all/Landscape/cropprogs/PLSpringBarleySpr.h \
    ../ALMaSS_all/Landscape/cropprogs/PLSpringWheat.h \
    ../ALMaSS_all/Landscape/cropprogs/PLWinterBarley.h \
    ../ALMaSS_all/Landscape/cropprogs/PLWinterRape.h \
    ../ALMaSS_all/Landscape/cropprogs/PLWinterRye.h \
    ../ALMaSS_all/Landscape/cropprogs/PLWinterTriticale.h \
    ../ALMaSS_all/Landscape/cropprogs/PLWinterWheat.h \
    ../ALMaSS_all/Landscape/cropprogs/PLWinterWheatLate.h \
    ../ALMaSS_all/Landscape/cropprogs/Potatoes.h \
    ../ALMaSS_all/Landscape/cropprogs/PotatoesIndustry.h \
    ../ALMaSS_all/Landscape/cropprogs/seedgrass1.h \
    ../ALMaSS_all/Landscape/cropprogs/seedgrass2.h \
    ../ALMaSS_all/Landscape/cropprogs/setaside.h \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarley.h \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleyCloverGrass.h \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleyCloverGrassStrigling.h \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleyPeaCloverGrassStrigling.h \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleyPTreatment.h \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleySeed.h \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleySilage.h \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleySKManagement.h \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleySpr.h \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleyStrigling.h \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleyStriglingCulm.h \
    ../ALMaSS_all/Landscape/cropprogs/SpringBarleyStriglingSingle.h \
    ../ALMaSS_all/Landscape/cropprogs/SpringRape.h \
    ../ALMaSS_all/Landscape/cropprogs/Sugarbeet.h \
    ../ALMaSS_all/Landscape/cropprogs/Triticale.h \
    ../ALMaSS_all/Landscape/cropprogs/WheatPControl.h \
    ../ALMaSS_all/Landscape/cropprogs/WinterBarley.h \
    ../ALMaSS_all/Landscape/cropprogs/WinterBarleyStrigling.h \
    ../ALMaSS_all/Landscape/cropprogs/winterrape.h \
    ../ALMaSS_all/Landscape/cropprogs/winterrapestrigling.h \
    ../ALMaSS_all/Landscape/cropprogs/WinterRye.h \
    ../ALMaSS_all/Landscape/cropprogs/WinterRyeStrigling.h \
    ../ALMaSS_all/Landscape/cropprogs/WinterWheat.h \
    ../ALMaSS_all/Landscape/cropprogs/WinterWheatStrigling.h \
    ../ALMaSS_all/Landscape/cropprogs/WinterWheatStriglingCulm.h \
    ../ALMaSS_all/Landscape/cropprogs/WinterWheatStriglingSingle.h \
    ../ALMaSS_all/Landscape/cropprogs/WWheatPControl.h \
    ../ALMaSS_all/Landscape/cropprogs/WWheatPToxicControl.h \
    ../ALMaSS_all/Landscape/cropprogs/WWheatPTreatment.h \
    ../ALMaSS_all/Landscape/cropprogs/YoungForest.h \
    ../ALMaSS_all/MarshFritillary/MarshFritillary_all.h \
    ../ALMaSS_all/MarshFritillary/MarshFritillary_Population_Manager.h \
    ../ALMaSS_all/MarshFritillary/MarshFritillaryHdrs.h \
    ../ALMaSS_all/Newt/Newt_Population_Manager.h \
    ../ALMaSS_all/Newt/Newt.h \
    ../ALMaSS_all/OliveMoth/lacewing.h \
    ../ALMaSS_all/OliveMoth/olivemoth.h \
    ../ALMaSS_all/Osmia/Osmia_Population_Manager.h \
    ../ALMaSS_all/Osmia/Osmia.h \
    ../ALMaSS_all/Partridge/Partridge_All.h \
    ../ALMaSS_all/Partridge/Partridge_Communication.h \
    ../ALMaSS_all/Partridge/Partridge_Covey.h \
    ../ALMaSS_all/Partridge/Partridge_Population_Manager.h \
    ../ALMaSS_all/Pipistrelle/Pipistrelle_Population_Manager.h \
    ../ALMaSS_all/Pipistrelle/Pipistrelle.h \
    ../ALMaSS_all/Rabbit/Rabbit_Population_Manager.h \
    ../ALMaSS_all/Rabbit/Rabbit.h \
    ../ALMaSS_all/RodenticideModelling/Rodenticide.h \
    ../ALMaSS_all/RodenticideModelling/RodenticidePredators.h \
    ../ALMaSS_all/RoeDeer/Roe_all.h \
    ../ALMaSS_all/RoeDeer/Roe_constants.h \
    ../ALMaSS_all/RoeDeer/Roe_pop_manager.h \
    ../ALMaSS_all/Skylark/skylarks_all.h \
    ../ALMaSS_all/Spider/spider_all.h \
    ../ALMaSS_all/Spider/SpiderPopulationManager.h \
    ../ALMaSS_all/Vole/GeneticMaterial.h \
    ../ALMaSS_all/Vole/Predators.h \
    ../ALMaSS_all/Vole/vole_all.h \
    ../ALMaSS_all/Vole/VolePopulationManager.h \
    ../ALMaSS_all/Landscape/calendar.h \
    ../ALMaSS_all/Landscape/CIPELandscapeMaker.h \
    ../ALMaSS_all/Landscape/configurator.h \
    ../ALMaSS_all/Landscape/croprotation.h \
    ../ALMaSS_all/Landscape/daylength.h \
    ../ALMaSS_all/Landscape/elements.h \
    ../ALMaSS_all/Landscape/farm.h \
    ../ALMaSS_all/Landscape/landscape.h \
    ../ALMaSS_all/Landscape/lowqueue.h \
    ../ALMaSS_all/Landscape/ls.h \
    ../ALMaSS_all/Landscape/map_cfg.h \
    ../ALMaSS_all/Landscape/maperrormsg.h \
    ../ALMaSS_all/Landscape/Misc.h \
    ../ALMaSS_all/Landscape/pesticide.h \
    ../ALMaSS_all/Landscape/plants.h \
    ../ALMaSS_all/Landscape/rastermap.h \
    ../ALMaSS_all/Landscape/tole_declaration.h \
    ../ALMaSS_all/Landscape/tov_declaration.h \
    ../ALMaSS_all/Landscape/treatment.h \
    ../ALMaSS_all/Landscape/weather.h \
    ../ALMaSS_all/qt/habitatmap.h \
    ../ALMaSS_all/qt/clickablelabel.h \
    ../ALMaSS_all/qt/mwhivetool.h \
    ../ALMaSS_all/qt/honeycombcell.h \
    ../ALMaSS_all/HoneyBee/hive.h \
    ../ALMaSS_all/HoneyBee/honeybeetypes.h \
    ../ALMaSS_all/HoneyBee/testlandscape.h \
    ../ALMaSS_all/qt/resourcebrowser.h \
    ../ALMaSS_all/HoneyBee/honeybeeconfigs.h \
    ../ALMaSS_all/HoneyBee/hbincludes.h \
    ../ALMaSS_all/HoneyBee/hbtools.h \
    ../ALMaSS_all/Landscape/cropprogs/NLBeet.h \
    ../ALMaSS_all/Landscape/cropprogs/NLBeetSpring.h \
    ../ALMaSS_all/Landscape/cropprogs/NLCabbage.h \
    ../ALMaSS_all/Landscape/cropprogs/NLCabbageSpring.h \
    ../ALMaSS_all/Landscape/cropprogs/NLCarrots.h \
    ../ALMaSS_all/Landscape/cropprogs/NLCarrotsSpring.h \
    ../ALMaSS_all/Landscape/cropprogs/NLCatchPeaCrop.h \
    ../ALMaSS_all/Landscape/cropprogs/NLGrassGrazed1.h \
    ../ALMaSS_all/Landscape/cropprogs/NLGrassGrazed1Spring.h \
    ../ALMaSS_all/Landscape/cropprogs/NLGrassGrazed2.h \
    ../ALMaSS_all/Landscape/cropprogs/NLGrassGrazedLast.h \
    ../ALMaSS_all/Landscape/cropprogs/NLMaize.h \
    ../ALMaSS_all/Landscape/cropprogs/NLMaizeSpring.h \
    ../ALMaSS_all/Landscape/cropprogs/NLPermanentGrassGrazed.h \
    ../ALMaSS_all/Landscape/cropprogs/NLPotatoes.h \
    ../ALMaSS_all/Landscape/cropprogs/NLPotatoesSpring.h \
    ../ALMaSS_all/Landscape/cropprogs/NLSpringBarley.h \
    ../ALMaSS_all/Landscape/cropprogs/NLSpringBarleySpring.h \
    ../ALMaSS_all/Landscape/cropprogs/NLTulips.h \
    ../ALMaSS_all/Landscape/cropprogs/NLWinterWheat.h \
    ../ALMaSS_all/HoneyBee/hbspreadsheet.h \
    ../ALMaSS_all/HoneyBee/hbweather.h
    
FORMS    += ../ALMaSS_all/qt/mainwindow.ui \
    ../ALMaSS_all/qt/mwhivetool.ui \
    ../ALMaSS_all/qt/resourcebrowser.ui

RESOURCES += \
    ../ALMaSS_all/qt/almass.qrc

DISTFILES += \
    ../ALMaSS_all/BatchALMaSS/ODDoxRefs.xls \
    ../ALMaSS_all/BatchALMaSS/ODDox_template.txt \
    ../ALMaSS_all/GooseManagement/GooseManagementODdox.md \
    ../ALMaSS_all/GooseManagement/GooseMemoryMapClassODdox.md \
    ../ALMaSS_all/Hare/Hare_Energetics_ODDox.txt \
    ../ALMaSS_all/Hare/Hare_ODD.txt \
    ../ALMaSS_all/Hare/Hare_ODDox.txt \
    ../ALMaSS_all/Hare/Hare_ODDox2.txt \
    ../ALMaSS_all/Hare/Hare_ODDox3.txt \
    ../ALMaSS_all/Hare/Hare_ODDox4.txt \
    ../ALMaSS_all/Hunters/HuntersODdox.txt \
    ../ALMaSS_all/MarshFritillary/MarshFritillary_ODDox.txt \
    ../ALMaSS_all/MarshFritillary/MarshFritillary_ODDox2.txt \
    ../ALMaSS_all/Newt/NewtODdox.md \
    ../ALMaSS_all/OliveMoth/LaceWing.cpp~ \
    ../ALMaSS_all/OliveMoth/LaceWing (Case Conflict).cpp~ \
    ../ALMaSS_all/OliveMoth/lacewing.h~ \
    ../ALMaSS_all/Partridge/CoveyBehaviour.doc \
    ../ALMaSS_all/Partridge/Partridge_ODDox.txt \
    ../ALMaSS_all/Partridge/Partridge_ODDox2.txt \
    ../ALMaSS_all/Partridge/PartridgeNotes.txt \
    ../ALMaSS_all/Pipistrelle/PipistrelleODdox.md \
    ../ALMaSS_all/Rabbit/RabbitODdox.md \
    ../ALMaSS_all/RodenticideModelling/RodenticideModel_ODdox.txt \
    ../ALMaSS_all/RodenticideModelling/RodenticideModel_ODdox.md \
    ../ALMaSS_all/RoeDeer/RoeDeer_ODDox.txt \
    ../ALMaSS_all/Skylark/Skylark_ODdox.txt \
    ../ALMaSS_all/Skylark/Skylark_ODdox2.txt \
    ../ALMaSS_all/Vole/Vole_ODDox.txt \
    ../ALMaSS_all/Vole/Vole_ODDox2.txt
